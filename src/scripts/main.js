var osirisDashboard = angular.module('OsirisDashboard', [
    'countTo',
    'ngRoute',
    'ui.bootstrap'
]);

osirisDashboard.config(function($routeProvider) {
    $routeProvider
        .when('/', {
            templateUrl: 'src/templates/monitoring.html',
            controller: 'MonitoringCtrl as monitoring'
        })

        .when('/settings', {
            templateUrl: 'src/templates/settings/index.html',
            controller: 'SettingsCtrl as settings'
        })

        .when('/security', {
            templateUrl: 'src/templates/security/index.html',
            controller: 'SecurityCtrl as security'
        })

        .when('/processes', {
            templateUrl: 'src/templates/processes/index.html',
            controller: 'ProcessesCtrl as processes'
        })

        .otherwise({ template: '<h2>Ошибка 404</h2>' });
});


// View Controlles
osirisDashboard.controller('LayoutCtrl', LayoutCtrl);
osirisDashboard.controller('SettingsCtrl', SettingsCtrl);
osirisDashboard.controller('MonitoringCtrl', MonitoringCtrl);
osirisDashboard.controller('ProcessesCtrl', ProcessesCtrl);
osirisDashboard.controller('SecurityCtrl', SecurityCtrl);

// Modal Controllers
osirisDashboard.controller('logInModalCtrl', logInModalCtrl);
osirisDashboard.controller('pullModalCtrl', pullModalCtrl);
osirisDashboard.controller('confirmModalCtrl', confirmModalCtrl);
osirisDashboard.controller('emergencyModalCtrl', emergencyModalCtrl);

function logInModalCtrl($modalInstance) {
    this.submit = function (loginForm) {
        if (loginForm.$valid) {
            $modalInstance.close(loginForm.login);
        }
    };

    this.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
}

function confirmModalCtrl($modalInstance, selectedPull, action, $modal) {
    this.action = action;
    this.buttonActionText = 'Удалить';

    this.submit = function(confirmForm) {
        if (confirmForm.$valid) {
            $modalInstance.close(selectedPull);
        }
    };

    this.cancel = function() {
        $modalInstance.dismiss('cancel');
    };
}

function pullModalCtrl($modalInstance, selectedPull, action) {
    this.data = {};
    this.action = action;
    this.buttonActionText = 'Создать';

    switch (action) {
        case 'add':
            this.buttonActionText = 'Создать';
            break;
        case 'edit':
            this.buttonActionText = 'Изменить';
            break;
        case 'delete':
            this.buttonActionText = 'Удалить';
            break;
    }

    if (selectedPull) {
        this.data = selectedPull;
    }

    this.submit = function (pullForm, pullModal) {
        if (pullForm.$valid) {
            $modalInstance.close(pullModal.data, $modalInstance);
        }
    };

    this.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
}

function emergencyModalCtrl($modalInstance, items) {
    this.items = items;

    this.close = function() {
        $modalInstance.dismiss('cancel');
    };
}

function LayoutCtrl($modal, $location) {
    var self = this;

    this.page = $location.$$path;

    this.logIn = function (size) {
        var logInModal = $modal.open({
            templateUrl: 'logInModal.html',
            controller: 'logInModalCtrl as logInModal',
            size: size
        });

        logInModal.result.then(function (login) {
            self.login = login.$modelValue;
        });
    };
}

function SecurityCtrl() {
    this.tab = 'manage-copy';

    this.pulls = [
        { name: 'RDB', reliability: 'x2', cache: '-',  volume: '100 ГБ', utilization: 90 },
        { name: 'RDB1', reliability: 'x2', cache: '-',  volume: '50 ГБ', utilization: 31 },
        { name: 'RDB2', reliability: 'x7', cache: '-',  volume: '30 ГБ', utilization: 15 },
        { name: 'RDB3', reliability: 'x8', cache: '-',  volume: '20 ГБ', utilization: 23 },
        { name: 'RDB4', reliability: 'x6', cache: '-',  volume: '15 ГБ', utilization: 12 },
        { name: 'RDB5', reliability: 'x2', cache: '-',  volume: '50 ГБ', utilization: 93 },
        { name: 'RDB6', reliability: 'x5', cache: '-',  volume: '10 ГБ', utilization: 43 },
        { name: 'RDB7', reliability: 'x2', cache: '-',  volume: '55 ГБ', utilization: 12 },
        { name: 'RDB8', reliability: 'x4', cache: '-',  volume: '60 ГБ', utilization: 54 },
        { name: 'RDB9', reliability: 'x2', cache: '-',  volume: '120 ГБ', utilization: 43 },
        { name: 'RDB10', reliability: 'x3', cache: '-',  volume: '200 ГБ', utilization: 12 }
    ];

    this.options = [
        { label: 'Пулы', value: 'RDB1' },
        { label: 'Таргеты', value: 'RDB2' },
        { label: 'Папки', value: 'RDB3' },
        { label: 'Пользователи', value: 'RDB4' }
    ];
}

function ProcessesCtrl() {
    this.tab = 'connects';

    this.connects = [
        { protocol: 'RDP', ip: '127.0.0.1', count: '0', time: '–' },
        { protocol: 'RTP', ip: '192.168.0.1', count: '2', time: '2 дня 13 часов 15 минут' },
        { protocol: 'HTTP', ip: '0.0.0.0', count: '4096', time: '42 дня 11 часов 20 минут' },
        { protocol: 'HTTPS', ip: '8.8.8.8', count: '403', time: '63 дня 13 часов 10 минут' },
        { protocol: 'SSH', ip: '127.0.0.1', count: '7', time: '2 дня 13 часов 15 минут' },
        { protocol: 'FTP', ip: '192.168.0.1', count: '3', time: '2 дня 13 часов 15 минут' },
        { protocol: 'POP3', ip: '192.168.0.1', count: '604', time: '2 дня 13 часов 15 минут' }
    ];

    this.process = [
        { name: 'image0/R', volume: '5 Гб', progress: 'Зеркалирование', goal: 'sys0' },
        { name: 'RSB', volume: '4 Гб', progress: '20%', goal: 'sys0' },
        { name: 'Pool', volume: '12 Гб', progress: 'Зеркалирование', goal: 'sys1' },
        { name: 'Pool', volume: '10 Гб', progress: '70%', goal: 'sys1' },
        { name: 'Pool#2', volume: '1 Гб', progress: 'Зеркалирование', goal: 'sys1' },
        { name: 'Pool#4', volume: '512 Мб', progress: '1%', goal: 'sys0' }
    ];
}

function SettingsCtrl($modal) {
    var self = this;

    this.tab = 'pulls';
    this.selectedPull = {};

    // Выбор пула
    this.selectPull = function ($index, pull) {
        if (self.selectedPull.index === $index) {
            self.selectedPull = {};
        } else {
            self.selectedPull.index = $index;
            self.selectedPull.data = pull;
        }
    };

    // Редактирование пула
    this.editPull = function (){
        if (self.selectedPull.data !== undefined) {
            self.pullModal('sm', 'edit', self.selectedPull.data);
        }
    };

    // Удаление пула
    this.deletePull = function() {
        if (self.selectedPull.data !== undefined) {
            self.pullModal('sm', 'delete', self.selectedPull.data);
        }
    };

    // Создание пула
    this.createPull = function() {
        self.pullModal('sm','add');
    };

    /**
     * Инициализация модального окна
     * @param size - размер окна
     * @param action - действие (создать, изменит, удалить)
     * @param pull - данные выбранного пула
     */
    this.pullModal = function (size, action, pull) {
        var pullModal = $modal.open({
            templateUrl: 'pullModal.html',
            controller: 'pullModalCtrl as pullModal',
            size: size,
            resolve: {
                action: function() {
                  return action;
                },
                selectedPull: function() {
                    return pull;
                }
            }
        });

        pullModal.result.then(function (pullModalData) {
            switch (action) {
                case 'add':
                    self.pulls.push({
                        name: pullModalData.name,
                        reliability: pullModalData.reliability,
                        cache: '-',
                        volume: '- ГБ',
                        utilization: 0
                    });
                    break;
                case 'edit':
                    break;
                case 'delete':
                    self.confirmModal('sm','delete', pullModalData);
                    break;
            }
        });
    };

    this.confirmModal = function(size, action, pull) {
        var confirmModal = $modal.open({
                templateUrl: 'confirmModal.html',
                controller: 'confirmModalCtrl as confirmModal',
                size: size,
                resolve: {
                    action: function() {
                        return action;
                    },
                    selectedPull: function() {
                        return pull;
                    }
                }
            });

        //then(successCallback, errorCallback, notifyCallback)
        confirmModal.result.then(function(pullModalData) {
            if (action === 'delete') {
                self.pulls.splice(self.pulls.indexOf(pullModalData), 1);
                self.selectedPull = {};
            }
        }, function() {
            self.deletePull();
        });
    };

    this.pulls = [
        { name: 'RDB', reliability: 'x2', cache: '-',  volume: '100 ГБ', utilization: 90 },
        { name: 'RDB1', reliability: 'x2', cache: '-',  volume: '50 ГБ', utilization: 31 },
        { name: 'RDB2', reliability: 'x7', cache: '-',  volume: '30 ГБ', utilization: 15 },
        { name: 'RDB3', reliability: 'x8', cache: '-',  volume: '20 ГБ', utilization: 23 },
        { name: 'RDB4', reliability: 'x6', cache: '-',  volume: '15 ГБ', utilization: 12 },
        { name: 'RDB5', reliability: 'x2', cache: '-',  volume: '50 ГБ', utilization: 93 },
        { name: 'RDB6', reliability: 'x5', cache: '-',  volume: '10 ГБ', utilization: 43 },
        { name: 'RDB7', reliability: 'x2', cache: '-',  volume: '55 ГБ', utilization: 12 },
        { name: 'RDB8', reliability: 'x4', cache: '-',  volume: '60 ГБ', utilization: 54 },
        { name: 'RDB9', reliability: 'x2', cache: '-',  volume: '120 ГБ', utilization: 43 },
        { name: 'RDB10', reliability: 'x3', cache: '-',  volume: '200 ГБ', utilization: 12 }
    ];

    this.targets = [
        { name: '495', figure: 'RBD/image1', security: 'Без резерва',  volume: '100 ГБ', used: 2 },
        { name: '496', figure: 'RBD/image2', security: 'HA/MPIO',  volume: '50 ГБ', used: 12 },
        { name: '497', figure: 'RBD/image2', security: 'Без резерва',  volume: '30 ГБ', used: 15 },
        { name: '498', figure: 'RBD/image1', security: 'HA/MPIO',  volume: '20 ГБ', used: 23 },
        { name: '499', figure: 'RBD/image16', security: 'Без резерва',  volume: '15 ГБ', used: 12 },
        { name: '500', figure: 'RBD/image2', security: 'HA/MPIO',  volume: '50 ГБ', used: 93 },
        { name: '501', figure: 'RBD/image4', security: 'HA/MPIO',  volume: '10 ГБ', used: 34 },
        { name: '502', figure: 'RBD/image2', security: 'HA/MPIO',  volume: '55 ГБ', used: 12 },
        { name: '503', figure: 'RBD/image1', security: 'HA/MPIO',  volume: '60 ГБ', used: 54 },
        { name: '504', figure: 'RBD/image4', security: 'Без резерва',  volume: '120 ГБ', used: 43 },
        { name: '505', figure: 'RBD/image4', security: 'Без резерва',  volume: '200 ГБ', used: 12 }
    ];

    this.folders = [
        { name: 'VMWARE', path: '/NFS/VMWARE', volume: '60 ГБ', used: 2 },
        { name: 'VMWARE(1)', path: '/NFS/VMWARE1', volume: '100 ГБ', used: 21 },
        { name: 'VMWARE(2)', path: '/NFS/VMWARE2', volume: '45 ГБ', used: 45 },
        { name: 'VMWARE(3)', path: '/NFS/VMWARE3', volume: '20 ГБ', used: 12 },
        { name: 'VMWARE(4)', path: '/NFS/VMWARE3', volume: '500 ГБ', used: 90 },
        { name: 'VMWARE(5)', path: '/NFS/VMWARE2', volume: '250 ГБ', used: 31 },
        { name: 'VMWARE(6)', path: '/NFS/VMWARE2', volume: '120 ГБ', used: 15 },
    ];

    this.users = [
        { name: 'admin', roles: ['mon', 'config'] },
        { name: 'user1', roles: ['mon', 'config'] },
        { name: 'user2', roles: ['mon', 'config', 'config:NFS'] },
        { name: 'account', roles: ['mon', 'config', 'config:NFS'] },
        { name: 'moder', roles: ['mon', 'config', 'config:NFS'] },
        { name: 'postgres', roles: ['mon'] },
        { name: 'mysql', roles: ['mon' ] }
    ];
}

function MonitoringCtrl($modal) {

    var self = this;

    this.emergencyModal = function (size, items) {

        var emergencyModal = $modal.open({
            templateUrl: 'emergencyModal.html',
            controller: 'emergencyModalCtrl as emergencyModal',
            size: size,
            resolve: {
                items: function() {
                    return items;
                }
            }
        });

        emergencyModal.result.then(function () {
        });
    };

    this.emergencyServers = [
        { 'name': 'Сервер №1', 'state': 'warning' },
        { 'name': 'Сервер №2', 'state': 'critical' },
        { 'name': 'Сервер №3', 'state': 'warning' },
        { 'name': 'Сервер №4', 'state': 'warning' }
    ];

    this.emergencyDisks = [
        { 'name': 'Диск №1', 'state': 'warning' },
        { 'name': 'Диск №2', 'state': 'critical' },
        { 'name': 'Диск №3', 'state': 'warning' },
        { 'name': 'Диск №4', 'state': 'warning' }
    ];

    // BarCharts
    setTimeout(function () {
        self.num1 = 15;
        self.num2 = 51;
        self.num3 = 3.6;
        self.num4 = 1;
        self.num5 = 15;
        self.num6 = 90;
    }, 500);


    // DoughnutCharts
    this.chart1 = 17;
    this.chart2 = 87;
    this.chart3 = 48;

    var options = {
        animationEasing: 'linear',
        animateRotate: true,
        percentageInnerCutout: 85,
        animationSteps: 50,
        responsive: true,
        segmentShowStroke: false
    };

    var $chart1 = $('#chart1'),
        $chart2 = $('#chart2'),
        $chart3 = $('#chart3');

    var ctx1 = $chart1[0].getContext('2d'),
        ctx2 = $chart2[0].getContext('2d'),
        ctx3 = $chart3[0].getContext('2d');

    var chart1 = new Chart(ctx1).Doughnut([
        {
            value: this.chart1,
            color: "#00AB3C",
            label: "#2"
        },
        {
            value: 100 - this.chart1,
            color: "#E8E8E8",
            label: "#1"
        }
    ], options);

    var chart2 = new Chart(ctx2).Doughnut([
        {
            value: this.chart2,
            color: "#AB0000",
            label: "#2"
        },
        {
            value: 100 - this.chart2,
            color: "#E8E8E8",
            label: "#1"
        }
    ], options);

    var chart3 = new Chart(ctx3).Doughnut([
        {
            value: this.chart3,
            color: "#FFC400",
            label: "#2"
        },
        {
            value: 100 - this.chart3,
            color: "#E8E8E8",
            label: "#1"
        }
    ], options);

}