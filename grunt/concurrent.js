module.exports = {

    // Настройки задач
    options: {
        limit: 3
    },

    // Задачи для разработки
    devFirst: [
        'clean',
        'jshint',
        'concat:dev'
    ],
    devSecond: [
        'copy:fonts',
        'stylus:dev'
    ],

    // Задачи для продакшна
    prodFirst: [
        'clean',
        'jshint',
        'concat:prod'
    ],
    prodSecond: [
        'stylus:prod',
        'copy:fonts',
        'copy:templates',
        'uglify:prod'
    ],

    // Image tasks
    imgFirst: [
        'imagemin'
    ]
};