module.exports = {

    options: {
        spawn: false,
        livereload: true
    },

    scripts: {
        files: [
            'src/scripts/*.js'
        ],
        tasks: [
            'jshint',
            'concat',
            //'uglify:dev'
        ]
    },

    styles: {
        files: [
            'node_modules/bootstrap-styl/bootstrap/*/*.styl',
            'node_modules/bootstrap-styl/bootstrap/*.styl',
            'src/styles/*.styl',
            'src/styles/*/*.styl'
        ],
        tasks: [
            'stylus:dev'
        ]
    },

    html: {
        files: [
            '*.html',
            'src/templates/*/*.html'
        ]
    }
};