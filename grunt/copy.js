module.exports = {
    fonts: {
        expand: true,
        flatten: true,
        filter: 'isFile',
        src: [
            "bower_components/bootstrap/fonts/*",
            "src/icomoon/fonts/*"
        ],
        dest: "dist/fonts"
    },
    templates: {
        expand: true,
        cwd: 'src/templates',
        src: [
            "**"
        ],
        dest: "dist/templates/",
        options: {
            process: function (content, srcpath) {
                return content.replace(/src\/templates/g, "dist/templates"); // replace path to templates on prod env
            }
        }
    }
};