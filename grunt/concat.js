var js_files = [
    'bower_components/jquery/dist/jquery.js',
    'bower_components/angular/angular.js',
    'bower_components/angular-bootstrap/ui-bootstrap.js',
    'bower_components/angular-bootstrap/ui-bootstrap-tpls.js',
    'bower_components/angular-route/angular-route.js',
    'bower_components/chartjs/Chart.js',
    'src/scripts/vendor/angular-count-to/src/count-to.js',
    'src/scripts/*.js'
];

module.exports = {
    options: {
        separator: ';'
    },
    dev: {
        src: js_files,
        dest: 'dist/scripts/main.js'
    },
    prod: {
        src: js_files,
        dest: 'dist/scripts/main.js',
        options: {
            process: function (src, filepath) {
                return src.replace(/src\/templates/g, "dist/templates"); // replace path to templates on prod env
            }
        }
    }

}