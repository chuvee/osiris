
module.exports = {
    dev: {
        options: {
            'include css': true,
            use: [
                require('nib')
            ],
            import: [
                'nib'
            ],
            compress: false,
            linenos: true
        },
        files: [{
            expand: true,
            cwd: 'src/styles',
            src: ['main.styl'],
            dest: 'dist/styles',
            ext: '.css'
        }]
    },
    prod: {
        options: {
            'include css': true,
            use: [
                require('nib')
            ],
            import: [
                'nib'
            ],
            compress: true,
            linenos: false
        },
        files: [{
            expand: true,
            cwd: 'src/styles',
            src: ['main.styl'],
            dest: 'dist/styles',
            ext: '.css'
        }]
    }
};